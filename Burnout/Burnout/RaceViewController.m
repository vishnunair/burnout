//
//  RaceViewController.m
//  Burnout
//
//  Created by Vishnu Nair on 30/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import "RaceViewController.h"
#import "AppDelegate.h"

@interface RaceViewController ()
{
    int countdownVal;
}

@property (nonatomic, strong) AppDelegate *appDelegate;

@end

@implementation RaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"Connected devices %@",_appDelegate.mcManager.session.connectedPeers);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveDataWithNotification:)
                                                 name:@"MCDidReceiveDataNotification"
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendMessage:(id)sender
{
    NSData *dataToSend = [[NSString stringWithFormat:@"HELLO WORLD"] dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *allPeers = _appDelegate.mcManager.session.connectedPeers;
    NSError *error;
    
    [_appDelegate.mcManager.session sendData:dataToSend
                                     toPeers:allPeers
                                    withMode:MCSessionSendDataReliable
                                       error:&error];
    
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self initializeTimer];
        });
    }
}

- (void)initializeTimer
{
    countdownVal = 10;
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
}

- (void)timerTick:(NSTimer *)timer
{
    countdownVal--;
    
    _lbl_countdown.text = [NSString stringWithFormat:@"%d",countdownVal];
    
    if(countdownVal==0)
    {
        [timer invalidate];
    }
}

- (IBAction)cancelMessage:(id)sender
{
    
}

-(void)didReceiveDataWithNotification:(NSNotification *)notification
{
    /*
    MCPeerID *peerID = [[notification userInfo] objectForKey:@"peerID"];
    NSString *peerDisplayName = peerID.displayName;
    
    NSData *receivedData = [[notification userInfo] objectForKey:@"data"];
    NSString *receivedText = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Message ---- %@ from %@", receivedText, peerDisplayName);
    
    [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"Received message - %@ - from %@",receivedText,peerDisplayName]];
     */
    dispatch_async(dispatch_get_main_queue(), ^{
        [self initializeTimer];
    });
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
