//
//  RaceViewController.h
//  Burnout
//
//  Created by Vishnu Nair on 30/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaceViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lbl_countdown;

- (IBAction)sendMessage:(id)sender;
- (IBAction)cancelMessage:(id)sender;

@end
