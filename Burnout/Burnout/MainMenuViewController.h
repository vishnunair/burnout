//
//  MainMenuViewController.h
//  Burnout
//
//  Created by Vishnu Nair on 30/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuViewController : UIViewController
- (IBAction)action_challenge:(id)sender;
- (IBAction)action_practice:(id)sender;
- (IBAction)action_timings:(id)sender;
- (IBAction)action_profile:(id)sender;
- (IBAction)action_settings:(id)sender;

@end
