//
//  PracticeSetupViewController.m
//  Burnout
//
//  Created by Vishnu Nair on 30/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import "PracticeSetupViewController.h"

@interface PracticeSetupViewController ()<PSLocationManagerDelegate>

@end

@implementation PracticeSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [PSLocationManager sharedLocationManager].delegate = self;
    [[PSLocationManager sharedLocationManager] prepLocationUpdates];
    [[PSLocationManager sharedLocationManager] startLocationUpdates];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark PSLocationManagerDelegate

- (void)locationManager:(PSLocationManager *)locationManager signalStrengthChanged:(PSLocationManagerGPSSignalStrength)signalStrength
{
    NSString *strengthText;
    if (signalStrength == PSLocationManagerGPSSignalStrengthWeak)
    {
        strengthText = NSLocalizedString(@"Weak", @"");
    }
    else if (signalStrength == PSLocationManagerGPSSignalStrengthStrong)
    {
        strengthText = NSLocalizedString(@"Strong", @"");
    }
    else
    {
        strengthText = NSLocalizedString(@"...", @"");
    }
    
    NSLog(@"Signal %@",strengthText);
}

- (void)locationManagerSignalConsistentlyWeak:(PSLocationManager *)locationManager
{
    //self.strengthLabel.text = NSLocalizedString(@"Consistently Weak", @"");
    NSLog(@"Signal %@",NSLocalizedString(@"Consistently Weak", @""));
}

- (void)locationManager:(PSLocationManager *)locationManager distanceUpdated:(CLLocationDistance)distance
{
    //self.distanceLabel.text = [NSString stringWithFormat:@"%.2f %@", distance, NSLocalizedString(@"meters", @"")];
    NSLog(@"Distance travelled %@",[NSString stringWithFormat:@"%.2f %@", distance, NSLocalizedString(@"meters", @"")]);
}

- (void)locationManager:(PSLocationManager *)locationManager error:(NSError *)error
{
    // location services is probably not enabled for the app
    //self.strengthLabel.text = NSLocalizedString(@"Unable to determine location", @"");
    NSLog(@"%@",NSLocalizedString(@"Unable to determine location", @""));
}


@end
