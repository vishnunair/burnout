//
//  LoginViewController.h
//  Burnout
//
//  Created by Vishnu Nair on 22/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textfield_email;
@property (weak, nonatomic) IBOutlet UITextField *textfield_password;
- (IBAction)action_login:(id)sender;
- (IBAction)action_signup:(id)sender;

@end
