//
//  AppDelegate.h
//  Burnout
//
//  Created by Vishnu Nair on 22/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) MCManager *mcManager;


@end

