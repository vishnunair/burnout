//
//  MainMenuViewController.m
//  Burnout
//
//  Created by Vishnu Nair on 30/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import "MainMenuViewController.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_challenge:(id)sender
{
    [self performSegueWithIdentifier:kSegue_MainScreenToChallengeScreen sender:nil];
}

- (IBAction)action_practice:(id)sender
{
    [self performSegueWithIdentifier:kSegue_MainScreenToPracticeScreen sender:nil];
}

- (IBAction)action_timings:(id)sender
{
    [self performSegueWithIdentifier:kSegue_MainScreenToTimingsScreen sender:nil];
}

- (IBAction)action_profile:(id)sender
{
    [self performSegueWithIdentifier:kSegue_MainScreenToProfileScreen sender:nil];
}

- (IBAction)action_settings:(id)sender
{
    [self performSegueWithIdentifier:kSegue_MainScreenToSettingsScreen sender:nil];
}
@end
