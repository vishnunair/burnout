//
//  SignUpViewController.h
//  Burnout
//
//  Created by Vishnu Nair on 22/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textfield_name;
@property (weak, nonatomic) IBOutlet UITextField *textfield_email;
@property (weak, nonatomic) IBOutlet UITextField *textfield_password;
@property (weak, nonatomic) IBOutlet UITextField *textfield_passwordConfirm;
- (IBAction)action_signup:(id)sender;

@end
