//
//  main.m
//  Burnout
//
//  Created by Vishnu Nair on 22/11/16.
//  Copyright © 2016 vishnunair. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
